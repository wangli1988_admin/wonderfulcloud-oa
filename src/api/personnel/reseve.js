import { axios } from '@/utils/request'
import system from '@/config/defaultSettings'

// 销售人员相关接口
const api = {
  pageList: '/reserve/pageList'
}

// 分页列表
export function pageList (param) {
  return axios({
    baseURL: system.baseURL,
    url: api.pageList,
    method: 'get',
    params: param
  })
}