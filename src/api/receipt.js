import { axios } from '@/utils/request'
import system from '@/config/defaultSettings'

/**
 * 获取收款单列表信息
 * @param parameter
 * @returns {*}
 */
export function getServiceList (parameter) {
  console.log('getServiceList manages' + JSON.stringify(parameter))
  return axios({
    baseURL: system.baseURL,
    url: '/receipt/getPageList',
    method: 'get',
    params: parameter
  })
}

/**
 * 去新增页面
 * @param parameter
 * @returns {*}
 */
export function goAdd (parameter) {
  console.log('goAdd manages' + JSON.stringify(parameter))
  return axios({
    baseURL: system.baseURL,
    url: '/receipt/goAdd',
    method: 'get',
    params: parameter
  })
}

/**
 * 收款单 查询合同列表
 * @param parameter
 * @returns {*}
 */
export function receiptSaleContract (parameter) {
  console.log('ReceiptSaleContract manages' + JSON.stringify(parameter))
  return axios({
    baseURL: system.baseURL,
    url: '/sale-contract/listBySaleReceipt',
    method: 'get',
    params: parameter
  })
}

/**
 * 所有的软件合同分页列表
 * @param {} parameter 
 */
export function allSoftwareContract (parameter) {
  return axios({
    baseURL: system.baseURL,
    url: '/software-contract/listBySaleReceipt',
    method: 'get',
    params: parameter
  })
}

/**
 * 获取合同详细信息
 * @param parameter
 * @returns {*}
 */
export function getContractOne (parameter) {
  return axios({
    baseURL: system.baseURL,
    url: '/sale-contract/queryOne',
    method: 'get',
    params: parameter
  })
}

/**
 * 获取开户行信息
 * @param parameter
 * @returns {*}
 */
export function getAccountBankList () {
  console.log('getAccountBankList manages')
  return axios({
    baseURL: system.baseURL,
    url: '/accountBank/queryList',
    method: 'get'
  })
}

/**
 * 添加收款单数据
 * @param parameter
 * @returns {*}
 */
export function save (parameter) {
  console.log('save parameter', JSON.stringify(parameter))
  return axios({
    baseURL: system.baseURL,
    url: '/receipt/save',
    method: 'post',
    data: parameter
  })
}

/**
 * 查看单据详细信息
 * @param parameter
 * @returns {*}
 */
export function receiptDetail (parameter) {
  console.log('receiptDetail ', JSON.stringify(parameter))
  const url = '/receipt/receiptDetail/' + parameter.id
  return axios({
    baseURL: system.baseURL,
    url: url,
    method: 'get'
  })
}
/**
 * 单据审核
 * @param parameter
 * @returns {*}
 */
export function receiptAudit (parameter) {
  console.log('receiptAudit ', JSON.stringify(parameter))
  return axios({
    baseURL: system.baseURL,
    url: '/receipt/receiptAudit',
    method: 'get',
    params: parameter
  })
}

/**
 * 删除收款单
 * @param parameter
 * @returns {*}
 */
export function deleteReceipt (parameter) {
  console.log('deleteReceipt ', JSON.stringify(parameter))
  const url = '/receipt/delete/' + parameter.id
  return axios({
    baseURL: system.baseURL,
    url: url,
    method: 'delete'
  })
}

/**
 * 更新收款单数据
 * @param parameter
 * @returns {*}
 */
export function updateReceipt (parameter) {
  console.log('updateReceipt parameter', JSON.stringify(parameter))
  return axios({
    baseURL: system.baseURL,
    url: 'receipt/updateReceipt',
    method: 'post',
    data: parameter
  })
}

/**
 * 根据软件合同id 获取合同产品信息
 * @param parameter
 * @returns {*}
 */
export function getSoftContractProductListById (parameter) {
  console.log('getSoftContractProductListById', JSON.stringify(parameter))
  return axios({
    baseURL: system.baseURL,
    url: 'software-contract/getSoftContractProductListById',
    method: 'get',
    params: parameter
  })
}


export function getSoftContractListById (parameter) {
  console.log('getSoftContractListById', JSON.stringify(parameter))
  return axios({
    baseURL: system.baseURL,
    url: 'software-contract/listBySaleReceipt',
    method: 'get',
    params: parameter
  })
}

//收款单，发货单，开票单，产品调度任务单：选择合同编号时，界面显示订单详情，下拉框带入 
export function productListByContract (parameter) {
  console.log('productListByContract', JSON.stringify(parameter))
  return axios({
    baseURL: system.baseURL,
    url: '/sale-contract/productListByContract',
    method: 'get',
    params: parameter
  })
}
